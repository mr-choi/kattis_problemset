#include <bits/stdc++.h>
using namespace std;
typedef vector<double> vd;

int main() {
    int p, a, b, c, d, n;
    cin >> p >> a >> b >> c >> d >> n;
    vd prices, highest_lefts;
    double highest_left = -1.0, lowest_right = 5000, highest_decline = 0;

    for (int k = 1; k <= n; k++)
    {
        double price = p*(sin(a*k+b)+cos(c*k+d)+2);
        prices.push_back(price);
        if (highest_left < price) highest_left = price;
        highest_lefts.push_back(highest_left);
    }
    for (int i = n-1; i >= 0; i--)
    {
        double price = prices[i];
        if (lowest_right > price) lowest_right = price;
        double decline = highest_lefts[i] - lowest_right;
        if (decline > highest_decline) highest_decline = decline;
    }
    cout << setprecision (9) << highest_decline << endl;
    return 0;
}