#include <bits/stdc++.h>
using namespace std;
typedef vector<char> vc;
typedef vector<int> vi;

struct UnionFind {
    vi p, rank;
    UnionFind(int n) {
        rank.assign(n,0); p.assign(n,0);
        for(int i=0; i<n; i++) p[i] = i;
    }
    int find(int i) {
        return (p[i]==i)?i:(p[i]=find(p[i]));
    }
    void unionSets(int i, int j) {
        if(find(i)!=find(j)) {
            int x = find(i), y = find(j);
            if (rank[x] > rank[y]) p[y] = x;
            else {
                p[x] = y;
                if(rank[x]==rank[y]) rank[y]++;
            }
        }
    }
};

vector<vi> visited;
vector<vc> grid;
int r, c;

int dr[] = {1, 0, -1, 0};
int dc[] = {0, -1, 0, 1};

void dfs(int i, int j, UnionFind &uf) {
    visited[i][j] = 1;
    for (int d=0; d<4; d++) {
        int i_new = i + dr[d], j_new = j + dc[d];
        if (visited[i_new][j_new]) continue;
        if (grid[i][j] != grid[i_new][j_new]) continue;
        uf.unionSets((c+2)*i + j, (c+2)*i_new + j_new);
        dfs(i_new, j_new, uf);
    }
}

int main() {
    cin >> r >> c;
    UnionFind uf ((r+2)*(c+2));
    visited.assign(r+2, vi(c+2, 0));

    grid.push_back(vc(c+2,'#'));
    for (int i=0; i<r; i++) {
        string bitrow;
        cin >> bitrow;
        vc row;
        row.push_back('#');
        for (char ch : bitrow) {
            row.push_back(ch);
        }
        row.push_back('#');
        grid.push_back(row);
    }
    grid.push_back(vc(c+2,'#'));

    for (int i=1; i<=r; i++) {
        for (int j=1; j<=c; j++) {
            if (!visited[i][j]) {
                dfs(i,j,uf);
            }
        }
    }

    int n;
    cin >> n;
    for (int i=0; i<n; i++) {
        int r1, c1, r2, c2;
        cin >> r1 >> c1 >> r2 >> c2;
        if (grid[r1][c1] == '0' && uf.find((c+2)*r1 + c1) == uf.find((c+2)*r2 + c2)) {
            cout << "binary" << endl;
        }
        else if (grid[r1][c1] == '1' && uf.find((c+2)*r1 + c1) == uf.find((c+2)*r2 + c2)) {
            cout << "decimal" << endl;
        }
        else {
            cout << "neither" << endl;
        }
    }
}