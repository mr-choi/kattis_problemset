# Kattis_problemset
This project contains c++ solutions of problems in [kattis](https://open.kattis.com/).
The directories are solutions of the given problem on kattis and can be found online by visiting https://open.kattis.com/problems/{name}.