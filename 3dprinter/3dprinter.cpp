#include <bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    if (n == 0) {
        cout << 0;
    } else {
        int d = (int)(ceil(log2(n)) + 1);
        cout << d;
    }
}