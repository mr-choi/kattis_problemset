#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int main () {
    int t;
    cin >> t;
    for (int i=0; i<t; i++) {
        int N;
        cin >> N;
        vi grades (N,0);
        int sum = 0;
        for (int j=0; j<N; j++) {
            int g;
            cin >> g;
            grades[j] = g;
            sum += g; 
        }
        int above_avg = 0;
        for (int j=0; j<N; j++) {
            if (grades[j]*N > sum) above_avg++;
        }
        double percentage = ((int)((above_avg * 100000.0) / N + 0.5))/1000.0;
        printf("%.3f%\n", percentage);
    }
}