#include <bits/stdc++.h>
using namespace std;

int main() {
    int numbers [3];
    string order;

    cin >> numbers[0] >> numbers[1] >> numbers[2];
    cin >> order;

    sort(numbers, numbers + 3);

    for (int i=0; i<3; i++) {
        cout << numbers[(int)(order[i]-'A')] << " ";
    }
}