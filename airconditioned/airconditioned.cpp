#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<ii> vii;

int main() {
    int M;
    cin >> M;
    ii minions [M];
    for (int i=0; i<M; i++) {
        int begin, end;
        cin >> begin >> end; 
        minions[i] = make_pair(begin,end);
    }
    sort(minions, minions + M);
    int rooms = 0;
    int minend = INT_MIN/2;
    for (int i=0; i<M; i++) {
        int newbegin = minions[i].first;
        int newend = minions[i].second;
        if (newbegin > minend) {
            rooms++;
            minend = newend;
        } else {
            minend = min(minend, newend);
        }
    }
    cout << rooms << endl;
}