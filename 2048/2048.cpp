#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int main() {
    int state [4][4];
    int dir;
    cin >> state[0][0] >> state[0][1] >> state[0][2] >> state[0][3];
    cin >> state[1][0] >> state[1][1] >> state[1][2] >> state[1][3];
    cin >> state[2][0] >> state[2][1] >> state[2][2] >> state[2][3];
    cin >> state[3][0] >> state[3][1] >> state[3][2] >> state[3][3];
    cin >> dir;

    int start [] = {0, 0, 3, 3};
    int stop [] = {4, 4, -1, -1};
    int step [] = {1, 1, -1, -1};

    for (int i=0; i<4; i++) {
        int moveback = start[dir];
        int lastnumber = -1;
        for (int j=start[dir]; j!=stop[dir]; j+=step[dir]) {
            int number = (dir % 2 == 0) ? state[i][j] : state[j][i];
            if (number==lastnumber) {
                (dir % 2 == 0) ? state[i][moveback-step[dir]] *= 2 : state[moveback-step[dir]][i] *= 2;
                (dir % 2 == 0) ? state[i][j] = 0 : state[j][i] = 0;
                lastnumber = -1;
            }
            else if (number > 0) {
                lastnumber = number;
                (dir % 2 == 0) ? state[i][j] = 0 : state[j][i] = 0;
                (dir % 2 == 0) ? state[i][moveback] = number : state[moveback][i] = number;
                moveback += step[dir];
            }
        }
    }

    cout << state[0][0] << " " << state[0][1] << " " << state[0][2] << " " << state[0][3] << endl;
    cout << state[1][0] << " " << state[1][1] << " " << state[1][2] << " " << state[1][3] << endl;
    cout << state[2][0] << " " << state[2][1] << " " << state[2][2] << " " << state[2][3] << endl;
    cout << state[3][0] << " " << state[3][1] << " " << state[3][2] << " " << state[3][3] << endl;
}